#ifndef WORKFLOW_WORKFLOWBLOCKINHERITED_H
#define WORKFLOW_WORKFLOWBLOCKINHERITED_H

#include <list>
#include "WorkflowBlock.h"
#include "WorkflowBlockFactory.h"


class ReadFileBlock : public WorkflowBlock  //������� �������������� ����� ��� ��������������� ������
{
public:
	void act(ReturnType lastBlockReturnType, std::list<std::string> *text, std::vector<std::string> params);
	ReturnType getReturnType() { return ReturnType::TEXT; }; //���������� �����
	int getParamAmount() { return 1; };						 //� ���� ��������
};

class WriteFileBlock : public WorkflowBlock
{
public:
	void act(ReturnType lastBlockReturnType, std::list<std::string> *text, std::vector<std::string> params);
	ReturnType getReturnType() { return ReturnType::NONE; };
	int getParamAmount() { return 1; };
};

class GrepBlock : public WorkflowBlock
{
public:
	void act(ReturnType lastBlockReturnType, std::list<std::string> *text, std::vector<std::string> params);
	ReturnType getReturnType() { return ReturnType::TEXT; };
	int getParamAmount() { return 1; };
};

class SortBlock : public WorkflowBlock 
{
public:
	void act(ReturnType lastBlockReturnType, std::list<std::string> *text, std::vector<std::string> params);
	ReturnType getReturnType() { return ReturnType::TEXT; };
	int getParamAmount() { return 0; };
};

class ReplaceBlock : public WorkflowBlock 
{
public:
	void act(ReturnType lastBlockReturnType, std::list<std::string> *text, std::vector<std::string> params);
	ReturnType getReturnType() { return ReturnType::TEXT; };
	int getParamAmount() { return 2; };
};
void addAllBlocksToFactory();

#endif //WORKFLOW_WORKFLOWBLOCKINHERITED_H
