#define CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>
#include <fstream>
#include <list>
using namespace std;
void sort_strings(list<string>*my_list)
{
	my_list->sort();
}
int main(int argc, char *argv[])
{
	ifstream* f_in = new ifstream;
	f_in->open(argv[0]);
	ofstream* f_out = new ofstream;
	f_out->open(argv[1]);
	list<string>* my_list = new list<string>;
	string str;
	while(getline(*f_in, str))
		my_list->push_back(str);
	sort_strings(my_list);
	while (!my_list->empty()) 
	{
		*f_out << my_list->front() << endl;
		my_list->pop_front();
	}
	f_in->close();
	f_out->close();
	delete my_list;
	delete f_in;
	delete f_out;
	system("pause");
	return 0;
}